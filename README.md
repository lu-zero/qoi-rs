

## Overview
qoi-rs is a QOI image encoder which uses nom for parsing. It currently outputs only to PNG.

### Run it

```sh
cargo run -i input.qoi -o output.png
```

or with `stdin` and `stdout`:

```sh
cargo run -- -i - -o - > output.png < input.qoi
```

### Roadmap

- [ ] Split into separate files
- [ ] Fuzzing
- [ ] Output to other formats
- [ ] Add QOI encoding

### Credits

Huge thanks to @lu-zero for giving feedback on both Nom and Rust ergonomics.
