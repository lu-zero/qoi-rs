use std::io::{stdin, stdout, Read, Result, Write};

use rgb::ComponentBytes;
use std::num::Wrapping;

use std::{fs, iter};

use nom::{
    bits::bits,
    bits::complete::{tag as bits_tag, take as bits_take},
    branch::alt,
    bytes::complete::tag,
    combinator::map,
    multi::many_till,
    number::streaming::{be_u32, be_u8},
    sequence::{preceded, tuple},
    IResult,
};

use rgb::RGBA8;

// for debug output

use std::fs::File;
use std::io::BufWriter;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the QOI input file
    #[arg(short, long)]
    input_file: String,

    /// Name of the PNG output file
    #[arg(short, long)]
    output_file: String,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Header {
    pub width: u32,
    pub height: u32,
    pub channels: u8,
    pub colorspace: u8,
}

// FIXME: use bitfields ?
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RGBDiff {
    pub dr: i8, // [-2..1] r channel difference from the previous pixel
    pub dg: i8, // [-2..1] g channel difference from the previous pixel
    pub db: i8, // [-2..1] b channel difference from the previous pixel
}

// FIXME: use bitfields ?
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct LumaDiff {
    pub dg: i8,    // [-32..31] 6-bit green channel difference from previous pixel
    pub dr_dg: i8, // [-8..7] 4-bit red channel difference minus green channel difference
    pub db_dg: i8, // [-8..7] 4-bit blue channel difference minus green channel difference
}

// https://qoiformat.org/qoi-specification.pdf
fn parse_header(qoi_buffer: &[u8]) -> IResult<&[u8], Header> {
    map(
        tuple((tag("qoif"), be_u32, be_u32, be_u8, be_u8)),
        |(_magic, width, height, channels, colorspace)| Header {
            width,      // image width in pixels (BE)
            height,     // image height in pixels (BE)
            channels,   // 3 = RGB, 4 = RGBA
            colorspace, // 0 = sRGB with linear alpha | 1 = all channels linear
        },
    )(qoi_buffer)
}

const QOI_OP_RGB_TAG: u8 = 0b11111110;
const QOI_OP_RGBA_TAG: u8 = 0b11111111;
const QOI_OP_INDEX_TAG: u8 = 0b00;
const QOI_OP_DIFF_TAG: u8 = 0b01;
const QOI_OP_LUMA_TAG: u8 = 0b10;
const QOI_OP_RUN_TAG: u8 = 0b11;

const QOI_BYTE_STREAM_END_TAG: [u8; 8] = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01];

fn index_hash(px: RGBA8) -> usize {
    ((px.r as u32 * 3 + px.g as u32 * 5 + px.b as u32 * 7 + px.a as u32 * 11) % 64) as usize
}

// See: https://adamchalmers.com/nom-bits/
type BitInput<'a> = (&'a [u8], usize);

// This is just a simple wrapper around the `tag` parser, but it makes the
// parameter types concrete instead of generic, so now Rust knows how to actually
// store the pattern.
fn tag_parser(pattern: u8, count: u8, input: BitInput) -> IResult<BitInput, u8> {
    bits_tag(pattern, count)(input)
}

fn take_n_bits(count: usize, input: BitInput) -> IResult<BitInput, u8> {
    bits_take(count)(input)
}

fn parse_rgb(qoi_buffer: &[u8]) -> IResult<&[u8], RGBA8> {
    let rgb_tag_parser = bits(|input| tag_parser(QOI_OP_RGB_TAG, 8, input));
    let rgb_parser = tuple((be_u8, be_u8, be_u8));

    map(preceded(rgb_tag_parser, rgb_parser), |(r, g, b)| RGBA8 {
        r,
        g,
        b,
        a: 255, /* alpha value remains unchanged from previous pixel */
    })(qoi_buffer)
}

fn parse_rgba(qoi_buffer: &[u8]) -> IResult<&[u8], RGBA8> {
    let rgba_tag_parser = bits(|input| tag_parser(QOI_OP_RGBA_TAG, 8, input));
    let rgba_parser = tuple((be_u8, be_u8, be_u8, be_u8));

    map(preceded(rgba_tag_parser, rgba_parser), |(r, g, b, a)| {
        RGBA8 { r, g, b, a }
    })(qoi_buffer)
}

fn parse_index(qoi_buffer: &[u8]) -> IResult<&[u8], u8> {
    let index_tag_parser = |input| tag_parser(QOI_OP_INDEX_TAG, 2, input);
    let index_parser = |input| take_n_bits(6usize, input);

    bits(move |input| preceded(index_tag_parser, index_parser)(input))(qoi_buffer)
}

fn parse_diff(qoi_buffer: &[u8]) -> IResult<&[u8], RGBDiff> {
    let diff_tag_parser = |input| tag_parser(QOI_OP_DIFF_TAG, 2, input);
    let take2bits = |input| take_n_bits(2usize, input);
    let diff_parser = move |input| tuple((take2bits, take2bits, take2bits))(input);

    map(
        bits(move |input| preceded(diff_tag_parser, diff_parser)(input)),
        |(dr, dg, db)| RGBDiff {
            dr: dr as i8 - 2,
            dg: dg as i8 - 2,
            db: db as i8 - 2,
        },
    )(qoi_buffer)
}

fn parse_luma_diff(qoi_buffer: &[u8]) -> IResult<&[u8], LumaDiff> {
    let luma_tag_parser = |input| tag_parser(QOI_OP_LUMA_TAG, 2, input);
    let take6bits = |input| take_n_bits(6usize, input);
    let take4bits = |input| take_n_bits(4usize, input);
    let luma_parser = move |input| tuple((take6bits, take4bits, take4bits))(input);

    map(
        bits(move |input| preceded(luma_tag_parser, luma_parser)(input)),
        |(dg, dr_dg, db_dg)| LumaDiff {
            dg: dg as i8 - 32,
            dr_dg: dr_dg as i8 - 8,
            db_dg: db_dg as i8 - 8,
        },
    )(qoi_buffer)
}

fn parse_run(qoi_buffer: &[u8]) -> IResult<&[u8], u8> {
    let run_tag_parser = |input| tag_parser(QOI_OP_RUN_TAG, 2, input);
    let run_parser = |input| take_n_bits(6usize, input);

    map(
        bits(move |input| preceded(run_tag_parser, run_parser)(input)),
        |run_length| run_length + 1,
    )(qoi_buffer)
}

fn apply_diff(px: &RGBA8, diff: &RGBDiff) -> RGBA8 {
    RGBA8 {
        r: (Wrapping(px.r as i8) + Wrapping(diff.dr)).0 as u8,
        g: (Wrapping(px.g as i8) + Wrapping(diff.dg)).0 as u8,
        b: (Wrapping(px.b as i8) + Wrapping(diff.db)).0 as u8,
        a: px.a,
    }
}

fn apply_luma_diff(px: &RGBA8, diff: &LumaDiff) -> RGBA8 {
    RGBA8 {
        r: (Wrapping(px.r as i8) + Wrapping(diff.dg + diff.dr_dg)).0 as u8,
        g: (Wrapping(px.g as i8) + Wrapping(diff.dg)).0 as u8,
        b: (Wrapping(px.b as i8) + Wrapping(diff.dg + diff.db_dg)).0 as u8,
        a: px.a,
    }
}

enum Chunk {
    Rgb(RGBA8),
    Rgba(RGBA8),
    Index(u8),
    Diff(RGBDiff),
    LumaDiff(LumaDiff),
    RunLength(u8),
}

fn decode(qoi_buffer: &[u8], width: u32, height: u32) -> IResult<&[u8], (Vec<RGBA8>, &[u8])> {
    map(
        many_till(
            alt((
                map(parse_rgb, Chunk::Rgb),
                map(parse_rgba, Chunk::Rgba),
                map(parse_index, Chunk::Index),
                map(parse_diff, Chunk::Diff),
                map(parse_luma_diff, Chunk::LumaDiff),
                map(parse_run, Chunk::RunLength),
            )),
            tag(QOI_BYTE_STREAM_END_TAG),
        ),
        // Given the parsed chunks, write pixels
        |(data, input)| {
            let mut previous_pixel = RGBA8 {
                r: 0,
                g: 0,
                b: 0,
                a: 255,
            };

            let mut previously_seen = [previous_pixel; 64];

            let mut result = Vec::with_capacity((width * height) as usize);

            for chunk in data.iter() {
                match chunk {
                    Chunk::Rgb(px) => {
                        // The alpha value remains unchanged from previous pixel.
                        let pixel = RGBA8 {
                            r: px.r,
                            g: px.g,
                            b: px.b,
                            a: previous_pixel.a,
                        };
                        result.push(pixel);
                        previous_pixel = pixel;
                        previously_seen[index_hash(pixel)] = pixel;
                    }
                    Chunk::Rgba(px) => {
                        result.push(*px);
                        previous_pixel = *px;
                        previously_seen[index_hash(*px)] = *px;
                    }
                    Chunk::Index(index) => {
                        let px = previously_seen[*index as usize];
                        result.push(px);
                        previous_pixel = px;
                        previously_seen[index_hash(px)] = px;
                    }
                    Chunk::Diff(diff) => {
                        let px = apply_diff(&previous_pixel, diff);
                        result.push(px);
                        previous_pixel = px;
                        previously_seen[index_hash(px)] = px;
                    }
                    Chunk::LumaDiff(diff) => {
                        let px = apply_luma_diff(&previous_pixel, diff);
                        result.push(px);
                        previous_pixel = px;
                        previously_seen[index_hash(px)] = px;
                    }
                    Chunk::RunLength(run_length) => {
                        result.extend(iter::repeat(previous_pixel).take((*run_length).into()));
                    }
                }
            }
            (result, input)
        },
    )(qoi_buffer)
}

fn save_output(path: &str, width: u32, height: u32, pixels: &[u8]) {
    let w: BufWriter<Box<dyn Write>> = match path {
        "-" => {
            // write to stdout
            BufWriter::new(Box::new(stdout()))
        }
        _ => {
            // write to file for any other path
            let file = File::create(path).unwrap();
            BufWriter::new(Box::new(file))
        }
    };

    let mut encoder = png::Encoder::new(w, width, height);
    encoder.set_color(png::ColorType::Rgba);
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(pixels).unwrap();
}

fn main() -> Result<()> {
    let args = Args::parse();

    let mut buffer = Vec::new();

    if args.input_file == "-" {
        // Read stdin
        stdin().read_to_end(&mut buffer).unwrap();
    } else {
        // Read file
        buffer = fs::read(args.input_file)?
    }

    let Ok((chunks, header)) = parse_header(&buffer) else { todo!() };

    let Ok((_leftover, (pixels, _leftover_px))) = decode(chunks, header.width, header.height) else { todo!() };

    assert!(pixels.len() == (header.width * header.height) as usize);

    save_output(
        &args.output_file,
        header.width,
        header.height,
        pixels.as_bytes(),
    );

    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_header() {
        let input: [u8; 16] = [
            0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x00,
            0x55, 0xfd,
        ];
        let Ok((_input, header)) = parse_header(&input) else { todo!() };
        assert!(
            header.width == 1247
                && header.height == 713
                && header.channels == 4
                && header.colorspace == 0
        )
    }

    #[test]
    #[should_panic]
    fn test_parse_invalid_magic() {
        let input: [u8; 16] = [
            0x72, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x00,
            0x55, 0xfd,
        ];
        let Ok((_input, _header)) = parse_header(&input) else { panic!() };
    }

    #[test]
    fn test_parse_rgb() {
        let input = [0b1111_1110, 0xaa, 0xbb, 0xcc];
        let Ok((_input, rgb)) = parse_rgb(&input) else { todo!() };
        assert!(rgb.r == 0xaa && rgb.g == 0xbb && rgb.b == 0xcc && rgb.a == 0xff)
    }

    #[test]
    fn test_parse_rgba() {
        let input = [0b1111_1111, 0xaa, 0xbb, 0xcc, 0xdd];
        let Ok((_input, rgba)) = parse_rgba(&input) else { todo!() };
        assert!(rgba.r == 0xaa && rgba.g == 0xbb && rgba.b == 0xcc && rgba.a == 0xdd)
    }

    #[test]
    fn test_index_hash() {
        assert!(
            index_hash(RGBA8 {
                r: 1,
                g: 2,
                b: 3,
                a: 4
            }) == 14
        )
    }

    #[test]
    fn test_parse_index() {
        let input = [0b00_00_00_10];
        let Ok((_input, index)) = parse_index(&input) else { todo!() };
        assert!(index == 0x02)
    }

    #[test]
    fn test_parse_diff() {
        let input: [u8; 1] = [0b01_00_10_11u8];
        let Ok((_input, drgb)) = parse_diff(&input) else { todo!() };
        assert!(drgb.dr == -2);
        assert!(drgb.dg == 0);
        assert!(drgb.db == 1);
    }

    #[test]
    fn test_apply_diff() {
        let neg_wrap_result = apply_diff(
            &RGBA8 {
                r: 1,
                g: 1,
                b: 1,
                a: 1,
            },
            &RGBDiff {
                dr: -2,
                dg: -2,
                db: -2,
            },
        );
        assert!(neg_wrap_result.r == 255);
        assert!(neg_wrap_result.g == 255);
        assert!(neg_wrap_result.b == 255);
        assert!(neg_wrap_result.a == 1); /* unchanged */

        let pos_wrap_result = apply_diff(
            &RGBA8 {
                r: 255,
                g: 255,
                b: 255,
                a: 255,
            },
            &RGBDiff {
                dr: 1,
                dg: 1,
                db: 1,
            },
        );
        assert!(pos_wrap_result.r == 0);
        assert!(pos_wrap_result.g == 0);
        assert!(pos_wrap_result.b == 0);
        assert!(pos_wrap_result.a == 255); /* unchanged */
    }

    #[test]
    fn test_parse_luma_diff() {
        let input: [u8; 2] = [0b10_000000u8, 0b0000_1111u8];
        let Ok((_input, dluma)) = parse_luma_diff(&input) else { todo!() };
        assert!(dluma.dg == -32);
        assert!(dluma.dr_dg == -8);
        assert!(dluma.db_dg == 7);
    }

    #[test]
    fn test_apply_luma_diff() {
        let neg_wrap_result = apply_luma_diff(
            &RGBA8 {
                r: 10,
                g: 10,
                b: 10,
                a: 10,
            },
            &LumaDiff {
                dg: -13,
                dr_dg: -13,
                db_dg: -13,
            },
        );
        assert!(neg_wrap_result.r == 240);
        assert!(neg_wrap_result.g == 253);
        assert!(neg_wrap_result.b == 240);
        assert!(neg_wrap_result.a == 10); /* unchanged */

        let pos_wrap_result = apply_luma_diff(
            &RGBA8 {
                r: 250,
                g: 250,
                b: 250,
                a: 250,
            },
            &LumaDiff {
                dg: 7,
                dr_dg: 7,
                db_dg: 7,
            },
        );
        assert!(pos_wrap_result.r == 8);
        assert!(pos_wrap_result.g == 1);
        assert!(pos_wrap_result.b == 8);
        assert!(pos_wrap_result.a == 250); /* unchanged */
    }

    #[test]
    fn test_parse_run() {
        // parse 2 runs
        let input: [u8; 2] = [0b11_000000u8, 0b11_111101u8];
        let Ok((rest, run)) = parse_run(&input) else { todo!() };
        assert!(run == 1);
        let Ok((_input, run)) = parse_run(&rest) else { todo!() };
        assert!(run == 62);
    }
}
